import mobile from './../images/mobile.jpg';
import design from './../images/design.jpg';
import website from './../images/website.jpg';

import jaskot from './../images/jaskot1.jpg';
import michal from './../images/michal.jpg';
import lukasz from './../images/lukasz.jpg';
import dominik from './../images/dominik.png';
import weronika from './../images/weronika.jpg';
import kaja from './../images/kaja.jpg';

export const products = [
    {
        textId: `home.products_webApp`,
        readMoreId: `home.products_readMore`,
        href: './',
        imgSrc: website
    },
    {
        textId: `home.products_mobileApp`,
        readMoreId: `home.products_readMore`,
        href: './',
        imgSrc: mobile
    },
    {
        textId: `home.products_design`,
        readMoreId: `home.products_readMore`,
        href: './',
        imgSrc: design
    },
];

export const services = [
    {
        header: `home.services_uxdesign_header`,
        text: `home.services_uxdesign_text`,
    },
    {
        header: `home.services_uigraphic_header`,
        text: `home.services_uigraphic_text`,
    },
    {
        header: `home.services_frontend_header`,
        text: `home.services_frontend_text`,
    },
    {
        header: `home.services_backend_header`,
        text: `home.services_backend_text`,
    },
];

export const ourTeam = [
    {
        name: `home.ourteam_1_name`,
        description: `home.ourteam_1_description`,
        imgSrc: lukasz,
    },
    {
        name: `home.ourteam_2_name`,
        description: `home.ourteam_2_description`,
        imgSrc: jaskot,
    },
    {
        name: `home.ourteam_3_name`,
        description: `home.ourteam_3_description`,
        imgSrc: dominik,
    },
    {
        name: `home.ourteam_4_name`,
        description: `home.ourteam_4_description`,
        imgSrc: weronika
    },
    {
        name: `home.ourteam_5_name`,
        description: `home.ourteam_5_description`,
        imgSrc: kaja
    },
    {
        name: `home.ourteam_6_name`,
        description: `home.ourteam_6_description`,
        imgSrc: michal
    },
];

export const behance = [
    {
        text: 'BEEHANCE',
        href: './'
    },
    {
        text: 'DRIBBLE',
        href: './'
    },
];