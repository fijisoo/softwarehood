import React from 'react';
import { FormattedMessage } from 'react-intl';

import withLayout from '../layout';

import './404.scss';

const NotFoundPage = () => (
  <div className="error404">
    <h1 className="error404-header">
      <FormattedMessage id="error404.NOT FOUND" />
    </h1>
    <p className="error404-description">
      <FormattedMessage id="error404.You just hit a route that doesnt exist the sadness" />
    </p>
  </div>
);

const customProps = {
  localeKey: 'error404',
  hideLangs: true,
};

export default withLayout(customProps)(NotFoundPage);
