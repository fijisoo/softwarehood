import React from 'react';

import withLayout from '../layout';
import {HeroBanner} from "../modules/HeroBanner";
import {Base} from "../modules/Base";
import {Products} from "../modules/Products";
import {Services} from "../modules/Services";
import {OurTeam} from "../modules/OurTeam";
import {HaveAProject} from "../modules/HaveAProject";

import './HomePage.scss';

const IndexPage = () => (
  <div className="HomePage">
      <HeroBanner />
      <Base />
      <Products/>
      <Services/>
      <OurTeam/>
      <HaveAProject/>
  </div>
);

const customProps = {
  localeKey: 'home', // same as file name in src/i18n/translations/your-lang/index.js
};

export default withLayout(customProps)(IndexPage);
