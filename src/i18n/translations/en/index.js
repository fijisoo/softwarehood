import flatten from 'flat';

import error404 from './404';
import home from './HomePage';
import {contact} from './Contact';

const message = { error404, home};

export default flatten(message);
