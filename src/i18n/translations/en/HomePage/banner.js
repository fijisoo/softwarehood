export default {
    banner: 'Build stunning design, websites & apps',
    description: 'A young but powerful team of designers and developers with one objective in mind. Making the web more friendly. Softwarehood was founded as a Polish based digital studio that focuses on developing high quality products to help your business succeed.'
};
