export default {
    services_heading: 'Our offer',
    services_subHeading: 'Services we offer',
    services_description: 'We offer a full range of design and development services. As our client, you’ll be in touch with our experienced project manager and our team, with updates provided and questions answered at every phase of the project.',
    services_uxdesign_header: 'UX Design',
    services_uxdesign_text: 'We understand the importance of a smooth, frictionless user journey. We’ll help you define business requirements, develop user profiles and workflows, and identify core functionality to create in-depth wireframes that will pave the way to a world-class user experience.',
    services_uigraphic_header: 'UI & Graphic Design',
    services_uigraphic_text: 'Great interface design is about delighting users with beautiful visuals and seamless, intuitive interactions. We’ll help you establish a consistent visual language that speaks to your brand, steps beyond passing trends, and keeps your users and customers coming back for more.',
    services_frontend_header: 'Front-end Development',
    services_frontend_text: 'Our front-end developers are highly experienced, fluent in multiple frameworks, and understand the importance of clean, eloquent code. We approach every project with longevity in mind, and our developers work closely with our designers to seamlessly bring ideas to life.',
    services_backend_header: 'Back-end development',
    services_backend_text: 'We understand that code is a living entity that needs to scale and adapt as your business grows and new information comes to light. We’ll help you pick the stack that fits the nature of your project and long-term goals, delivering a final product that prioritizes maintainability.',
};
