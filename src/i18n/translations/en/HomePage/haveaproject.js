export default {
    haveaproject_heading: 'Have a project for us?',
    haveaproject_description: 'Have a product that need some design love or a vision to change the world? Whatever problems you have, we’d love to hear about it.',
};
