export default {
    footer_sectionLeft: 'We’re Softwarehood. A young but powerful team of developers and designers with one objective in mind. Making the web more friendly.',
    footer_offer: 'Offer',
    footer_caseStudies: 'Case Studies',
    footer_ourTeam: 'Our team',
    footer_dribble: 'Dribble',
    footer_behance: 'Behance',
    footer_instagram: 'Instagram',
    footer_allRights: 'All rights reserved'
};
