export default {
    whatisitabout_header: 'What is it about?',
    whatisitabout: 'We are a group of  friends who creates digital products and brands.',
    whoisitfor_header: 'Who is it for?',
    whoisitfor: 'Softwarehood is a perfrect choice for startups that want to fully realize their ambitions.',
};
