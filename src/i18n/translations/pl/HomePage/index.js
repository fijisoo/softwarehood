import banner from './banner';
import nav from './nav';
import base from './base';
import products from './products';
import footer from './footer';
import services from './services';
import ourteam from "./ourteam";
import haveaproject from "./haveaproject";

const home = Object.assign({}, banner, nav, base, products, footer, services, ourteam, haveaproject);

export default home;