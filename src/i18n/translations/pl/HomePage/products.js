export default {
    products_heading: 'Our products',
    products_subHeading: 'What can we do for you?',
    products_webApp: 'Web app & websites',
    products_mobileApp: 'Mobile app',
    products_design: 'Design',
    products_readMore: 'WINCYJ →',
};
