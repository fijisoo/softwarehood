import React from 'react';
import {FormattedMessage} from "react-intl";
import {SectionHeading} from "../../components/SectionHeading";
import {ServicesBox} from "../../components/ServicesBox";
import {services} from '../../mocks/homePage';

import './Services.scss';

export const Services = () => {
    return (
        <div className="Services" id="Services">
            <div className="Services__heading">
                <SectionHeading heading={<FormattedMessage id='home.services_heading'/>}
                                subHeading={<FormattedMessage id='home.services_subHeading'/>}
                />
                <span className="Services__heading-description">
                    <FormattedMessage id='home.services_description'/>
                </span>
            </div>
            <div className="Services__content">
                {services.map(service => <ServicesBox className="Services__content-ServiceBox" text={<FormattedMessage id={service.text}/>} header={<FormattedMessage id={service.header}/>}/>)}
            </div>
        </div>
    )
};