import React from 'react';
import {FormattedMessage} from "react-intl";
import {Link} from 'gatsby';

import './Footer.scss';

const Footer = () => {
    return (
        <footer className="Footer">
            <div className="Footer__section">
                <div className="Footer__section-left">
                    <FormattedMessage id='home.footer_sectionLeft'/>
                </div>
                <div className="Footer__section-right">
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_offer'/></Link>
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_caseStudies'/></Link>
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_ourTeam'/></Link>
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_dribble'/></Link>
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_behance'/></Link>
                    <Link to={'/'} className="Footer__section-link" ><FormattedMessage id='home.footer_instagram'/></Link>
                </div>

            </div>
            <span className="Footer__background"/>
            <p className="Footer__text">
                © {new Date().getFullYear()} Softwarehood. <FormattedMessage id='home.footer_allRights'/>
            </p>
        </footer>
    )
};

export default Footer;