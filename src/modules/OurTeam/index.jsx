import React from 'react';
import {FormattedMessage} from "react-intl";
import {SectionHeading} from "../../components/SectionHeading";
import {Picture} from "../../components/Picture";
import Carousel from "nuka-carousel";

import {ourTeam} from "../../mocks/homePage";

import './OurTeam.scss';

const Slides = ourTeam.map(person => <Picture className="OurTeam__content-Picture" imgSrc={person.imgSrc} description={<FormattedMessage id={person.description}/>} name={<FormattedMessage id={person.name}/>}/>);

export const OurTeam = () => {

    return (<div className="OurTeam" id="OurTeam">
        <div className="OurTeam__heading">
            <SectionHeading heading={<FormattedMessage id='home.ourteam_heading'/>}
                            subHeading={<FormattedMessage id='home.ourteam_subHeading'/>}
            />
            <span className="OurTeam__heading-description">
                <FormattedMessage id='home.ourteam_description'/>
            </span>
        </div>
        <div className="OurTeam__content">
            <Carousel
              className="OurTeam__content-Carousel"
              autoplay
              withoutControls
              autoplayInterval={4000}
              speed={800}
              slidesToShow={4}
              wrapAround
              slidesToScroll={1}
              transitionMode="scroll"
            >
                {[Slides]}
            </Carousel>
        </div>
    </div>)
};