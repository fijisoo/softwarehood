import React from 'react';
import {FormattedMessage} from "react-intl";
import LetsChat from "../../components/LetsChat";

import './HaveAProject.scss';

export const HaveAProject = () => {
    return (<div className="HaveAProject" id="HaveAProject">
        <div className="HaveAProject-heading">
            <FormattedMessage id='home.haveaproject_heading'/>
        </div>
        <div className="HaveAProject-description">
            <FormattedMessage id='home.haveaproject_description'/>
        </div>
        <div className="HaveAProject-LetsChat">
            <LetsChat>
                <FormattedMessage id="home.letsChat" />
            </LetsChat>
        </div>
    </div>)
}