import React from 'react';
import {FormattedMessage} from "react-intl";
import {SectionHeading} from "../../components/SectionHeading";
import ProductsCard from "../../components/ProductsCard";
import Carousel from 'nuka-carousel';

import {products} from "../../mocks/homePage";

import './Products.scss';


export const Products = () => {
    return (
        <div className="Products" id="Products">
            {/*<span className="Products__background"/>*/}
            <div className="Products__heading">
                <SectionHeading heading={<FormattedMessage id='home.products_heading'/>}
                                subHeading={<FormattedMessage id='home.products_subHeading'/>}
                />
            </div>
            <div className="Products__section">
                <Carousel slideWidth="300px" withoutControls slidesToShow={3} framePadding="20px 0" cellSpacing={23}>
                    {products.map((props) => <ProductsCard {...props}/>)}
                </Carousel>
            </div>
        </div>
    )
};