import React from 'react';
import LangButtons from './../../components/Langs';
import Behance from './../../components/Behance';
import {FormattedMessage} from "react-intl";
import {behance} from "../../mocks/homePage";

import './HeroBanner.scss';

export const HeroBanner = () => {
    return (
        <div className="HeroBanner">
            {/*<LangButtons className="HeroBanner__langs"/>*/}
            {/*<Behance className="HeroBanner__behance" data={behance} />*/}
            <h1 className="HeroBanner__header">
                <FormattedMessage id='home.banner' />
            </h1>
            <span className="HeroBanner__description">
                <FormattedMessage id='home.description' />
            </span>
        </div>
    )
}