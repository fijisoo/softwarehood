import React from 'react';
import {FormattedMessage} from "react-intl";

import './Base.scss';

export const Base = () => {
    return (
        <div className="Base">
            <span className="Base__background"/>
            <div className="Base__section">
                <div className="Base__section-heading">
                    <FormattedMessage id='home.whatisitabout_header' />
                </div>
                <FormattedMessage id='home.whatisitabout' />
            </div>
            <div className="Base__section">
                <div className="Base__section-heading">
                    <FormattedMessage id='home.whoisitfor_header' />
                </div>
                <FormattedMessage id='home.whoisitfor' />
            </div>
        </div>
    )
};