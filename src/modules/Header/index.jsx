import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../components/Link';
import LetsChat from "../../components/LetsChat";
import {FormattedMessage} from "react-intl";

import './Header.scss';

const Header = ({ siteTitle }) => (
  <header className="Header">
    <nav className="Header__nav">
        <div className="Header__nav-section Header__nav-section--left">
            <Link to="#Services" className="Header__nav-section-link">
                <FormattedMessage id="home.offer" />
            </Link>
            <Link to="/page-2/" className="Header__nav-section-link">
                <FormattedMessage id="home.caseStudies" />
            </Link>
        </div>
        <span className="Header__nav-title">{siteTitle}</span>
        <div className="Header__nav-section Header__nav-section--right">
            <Link to="#OurTeam" className="Header__nav-section-link">
                <FormattedMessage id="home.ourTeam" />
            </Link>
            <LetsChat className="Header__nav-section-letsChat">
                <FormattedMessage id="home.letsChat" />
            </LetsChat>
        </div>
    </nav>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
  hideLangs: PropTypes.bool,
};

Header.defaultProps = {
  siteTitle: ``,
  hideLangs: false,
};

export default Header;
