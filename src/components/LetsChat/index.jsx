import React from 'react';
import Link from "../Link";

import './LetsChat.scss';

const LetsChat = ({children, className}) => {
    // return <Link to="/page-2/" className={`LetsChat ${className ? className : ''}`}>
    //     {children}
    // </Link>
    return <a href="mailto:lukasz@swhood.com" className={`LetsChat ${className ? className : ''}`}>
        {children}
    </a>
};

export default LetsChat;