import React from 'react';
import classnames from 'classnames';

import './ServicesBox.scss';

export const ServicesBox = ({header, text, className}) => {
    return (
        <div className={classnames('ServicesBox', className)}>
            <div className="ServicesBox-header">
                {header}
            </div>
            <div className="ServicesBox-text">
                {text}
            </div>
        </div>
    )
};