import React from 'react';

import './SectionHeading.scss';

export const SectionHeading = ({heading, subHeading}) => {
    return (
        <div className="SectionHeading">
            {heading && <span className="SectionHeading__heading">
                {heading}
            </span>}
            <span className="SectionHeading__subHeading">
                {subHeading}
            </span>
        </div>
    )
};