import React from 'react';
import {Link} from 'gatsby';
import {injectIntl} from 'react-intl';

import './Behance.scss';

const Button = ({text, href}) => (
    <Link to={href} className="Behance-single">
        {text}
    </Link>
);

const Index = ({data, className}) => {
    return (
        <div className={`Behance ${className ? className : ''}`}>
            {data.map(props => (<Button {...props} />))}
        </div>
    );
};

export default injectIntl(Index);
