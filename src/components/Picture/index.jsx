import React from 'react';
import classnames from 'classnames';

import './Picture.scss';
import Image from '../Image';

export const Picture = ({imgSrc, name, description, className}) => {

    return (
        <span className={classnames('Picture', className)}>
            <Image className="Picture-img" imgSrc={imgSrc} responsive="width" fitted/>
            <span className="Picture-content">
                <span className="Picture-content-name">
                    {name}
                </span>
                <span className="Picture-content-description">
                    {description}
                </span>
            </span>
        </span>
    )
};