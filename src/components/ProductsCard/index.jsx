import React from 'react';
import Link from "../Link";
import Image from "../../components/Image";
import { FormattedMessage } from "react-intl";

import './ProductsCard.scss';

const ProductsCard = ({href, imgSrc, textId, className, readMoreId}) => {
    return (
        <Link to={href} className={`ProductsCard ${className ? className : ''}`}>
            <Image className="ProductsCard-img" imgSrc={imgSrc} responsive="width" fitted/>
            <div className="ProductsCard-section">
                <div className="ProductsCard-section-text">
                    <FormattedMessage id={textId}/>
                </div>
                <div className="ProductsCard-section-readMore">
                    <FormattedMessage id={readMoreId}/>
                </div>
            </div>
        </Link>);
};

export default ProductsCard;