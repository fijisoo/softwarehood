import React from 'react';
import classnames from 'classnames';

import './Image.scss';

const Image = (props) => {
    const { imgSrc, alt, title, rounded, className, responsive, fitted } = props;

    const imageStyles = classnames(
        'Image',
        {
            [`Image--responsive-${responsive}`]: !!responsive,
            [`Image--rounded-${rounded}`]: !!rounded,
            'Image--fitted': fitted,
        },
        className,
    );

    return <img alt={alt} src={imgSrc} title={title} className={imageStyles} />;
};

export default Image;