import React, { useContext } from 'react';
import { navigate } from 'gatsby';
import { injectIntl } from 'react-intl';

import languages from '../../i18n/languages';
import PageContext from '../../layout/PageContext';

import './Langs.scss';

const LangButton = ({ label, active, onClick }) => (
  <button
    onClick={onClick}
    className="LangButtons-single"
    style={{
      fontWeight: active ? '500' : '400',
    }}
  >
    {label === 'Polski' ? 'PL' : 'EN'}
  </button>
);

const Index = ({ intl: { locale }, className}) => {
  const pageContext = useContext(PageContext);

  const handleSetLang = language => {
    const { originalPath } = pageContext.page;
    const newPathname = `/${language}${originalPath}`;

    localStorage.setItem('language', language);
    navigate(newPathname);
  };

  if (!pageContext.custom.localeKey) return null;
  return (
    <div className={`LangButtons ${className ? className : ''}`}>
      {languages.map(language => (
        <LangButton
          key={language.locale}
          label={language.label}
          active={language.locale === locale}
          onClick={() => handleSetLang(language.locale)}
        />
      ))}
    </div>
  );
};

export default injectIntl(Index);
