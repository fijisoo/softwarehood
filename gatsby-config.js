module.exports = {
    siteMetadata: {
        title: `Softwarehood`,
        description: `Softwarehood - your software next door`,
        author: `Dominik Beń @softwarehood`,
        keywords: [`softwarehood`, `application`, `react`, `web`, `design`],
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        `gatsby-plugin-sass`,
        // {
        //     resolve: 'gatsby-plugin-web-font-loader',
        //     options: {
        //         google: {
        //             families: ['Source Sans Pro', 'Nunito']
        //         }
        //     }
        // },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Softwarehood`,
                short_name: `Softwarehood`,
                start_url: `/`,
                background_color: `#fff`,
                theme_color: `#ea8fff`,
                display: `minimal-ui`,
                icon: `src/images/logo.png`, // This path is relative to the root of the site.
            },
        },
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.app/offline
        // 'gatsby-plugin-offline',
    ],
};
